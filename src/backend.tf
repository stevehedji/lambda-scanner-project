# Configure the backend to use S3 (To store the terraform state file)
terraform {
  backend "s3" {
    region     = "ap-southeast-2"
    encrypt    = true
  }
}