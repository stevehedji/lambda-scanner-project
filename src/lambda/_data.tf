# Permissions for the scanner function
data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid    = "CreateLogGroup"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
  statement {
    sid       = "AllowVPC"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ec2:Create*",
      "ec2:Describe*",
      "ec2:DeleteNetworkInterface"

    ]
  }
  statement {
    sid       = "AllowSSM"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ssm:GetParameter",
    ]
  }
  statement {
    sid       = "AllowSTS"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "sts:GetCallerIdentity"
    ]
  }
  statement {
    sid       = "AllowSES"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ses:SendEmail",
      "ses:SendRawEmail"
    ]
  }
  statement {
    sid       = "AllowLambda"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "lambda:get*",
      "lambda:list*"
    ]
  }
}

# Policy document for the scanner function
data "aws_iam_policy_document" "lambda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"
      ]
    }
  }
}