# IAM Role for the scanner
resource "aws_iam_role" "lambda_role" {
  name                  = "lambda-role-${var.function_name}"
  assume_role_policy    = data.aws_iam_policy_document.lambda_assume_role_policy.json
  force_detach_policies = true
  max_session_duration  = "3600"
}

# IAM policy for the scanner
resource "aws_iam_policy" "lambda_role_policy" {
  name   = "lambda-policy-${var.function_name}"
  path   = "/"
  policy = data.aws_iam_policy_document.lambda_policy.json
}

# Policy attachment for the scanner
resource "aws_iam_role_policy_attachment" "lambda_role_attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_role_policy.arn
}

