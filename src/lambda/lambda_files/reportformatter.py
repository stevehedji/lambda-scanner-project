import json
from fpdf import FPDF
import datetime

class ReportFormatter:

    # format_report
    # a function to format a bandit report to a PDF file
    # inputs:
    # report: dict - the dictionary object containing the unformatted report
    # acc_no: str - the account number for the report
    def format_report(report: dict, acc_no: str):
    
        pdf = FPDF()  
        pdf.add_page()
        pdf.set_font("Arial", size = 15)
        datenow = datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
        formatted_report = {}
        totals = [report['metrics']['_totals']['SEVERITY.HIGH'],report['metrics']['_totals']['SEVERITY.MEDIUM'],report['metrics']['_totals']['SEVERITY.LOW']]
        # Read the report Json and organise the data needed for the report
        for x in report['metrics']:
            if x != "_totals":
                funcname = x.replace('/tmp/zips/', '').split("/", 1)
                if funcname[0] not in formatted_report:
                    formatted_report[funcname[0]] = {}
                if 'SEVERITY.HIGH' not in formatted_report[funcname[0]]:
                    formatted_report[funcname[0]]['SEVERITY.HIGH'] = report['metrics'][x]['SEVERITY.HIGH']
                    formatted_report[funcname[0]]['SEVERITY.MEDIUM'] = report['metrics'][x]['SEVERITY.MEDIUM']
                    formatted_report[funcname[0]]['SEVERITY.LOW'] = report['metrics'][x]['SEVERITY.LOW']
                else:
                    formatted_report[funcname[0]]['SEVERITY.HIGH'] += report['metrics'][x]['SEVERITY.HIGH']
                    formatted_report[funcname[0]]['SEVERITY.MEDIUM'] += report['metrics'][x]['SEVERITY.MEDIUM']
                    formatted_report[funcname[0]]['SEVERITY.LOW'] += report['metrics'][x]['SEVERITY.LOW']
        resno = 0
        for x in report['results']:
            funcname = x['filename'].replace('/tmp/zips/', '').split("/", 1)
            if 'cwe' not in formatted_report[funcname[0]]:
                formatted_report[funcname[0]]['cwe'] = {}
            if x["issue_severity"] not in formatted_report[funcname[0]]['cwe']:
                formatted_report[funcname[0]]['cwe'][x["issue_severity"]] = {}
            if x['issue_cwe']['id'] not in formatted_report[funcname[0]]['cwe'][x["issue_severity"]]:
                formatted_report[funcname[0]]['cwe'][x["issue_severity"]][x['issue_cwe']['id']] = {"link":x['issue_cwe']['link'], "files":{}}
            if funcname[1] not in formatted_report[funcname[0]]['cwe'][x["issue_severity"]][x['issue_cwe']['id']]['files']:
                formatted_report[funcname[0]]['cwe'][x["issue_severity"]][x['issue_cwe']['id']]['files'][funcname[1]] = []
            
            formatted_report[funcname[0]]['cwe'][x["issue_severity"]][x['issue_cwe']['id']]['files'][funcname[1]].append(f"{x['line_number']}")
        
        # Generate the PDF file from the report
        pdf.image("/var/task/logo.png", w=75)
        pdf.ln(10)
        pdf.set_text_color(65,74,124)
        pdf.set_font("Arial", size = 14, style="B")
        outstr = f"Report Date: {datenow}"
        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        pdf.set_text_color(0,0,0)
        pdf.set_font("Arial", size = 12, style="B")
        outstr = f"Account: {acc_no}"
        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        outstr = f"High Severity: {totals[0]}"
        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        outstr = f"Medium Severity: {totals[1]}"
        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        outstr = f"Low Severity: {totals[2]}"
        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        pdf.ln(10)
        for x in formatted_report:
            if 'cwe' in formatted_report[x]:
                pdf.set_text_color(65,74,124)
                pdf.set_font("Arial", size = 14, style="BU")
                outstr = f"{x}"
                pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
                pdf.set_text_color(0,0,0)
                pdf.set_font("Arial", size = 10, style="B")
                if formatted_report[x]['SEVERITY.HIGH'] != 0:
                    outstr = f"HIGH Severity: {formatted_report[x]['SEVERITY.HIGH']}"
                    pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
                if formatted_report[x]['SEVERITY.MEDIUM'] != 0:
                    outstr = f"MEDIUM Severity: {formatted_report[x]['SEVERITY.MEDIUM']}"
                    pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
                if formatted_report[x]['SEVERITY.LOW'] != 0:
                    outstr = f"LOW Severity: {formatted_report[x]['SEVERITY.LOW']}"
                    pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
                pdf.set_font("Arial", size = 12)
                outstr = "Results:"
                pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
                pdf.set_font("Arial", size = 10)
                for y in formatted_report[x]['cwe']:
                    for z in formatted_report[x]['cwe'][y]:
                        if y == "LOW":
                            pdf.set_text_color(0,70,0)
                        elif y == "MEDIUM":
                            pdf.set_text_color(70,70,0)
                        elif y == "HIGH":
                            pdf.set_text_color(70,0,0)
                        outstr = f"{y} - CWE{z}: {formatted_report[x]['cwe'][y][z]['link']}"
                        pdf.cell(200,10, txt = outstr, ln = 1, align = 'L', link=formatted_report[x]['cwe'][y][z]['link'])
                        pdf.set_text_color(0,0,0)
                        for l in formatted_report[x]['cwe'][y][z]['files']:
                            outstr = f" - {l}:"
                            line_max = 0
                            has_multiline = False
                            for m in formatted_report[x]['cwe'][y][z]['files'][l]:
                                outstr = outstr + f"{m}, \n"
                                line_max += 1
                                if line_max > 18:
                                    pdf.cell(200,5, txt = outstr, ln = 1, align = 'L')
                                    has_multiline = True
                                    outstr = "      "
                                    line_max = 0
                            if has_multiline:
                                outstr = f"      + {outstr}"
                            else:
                                pdf.cell(200,5, txt = outstr, ln = 1, align = 'L')
                    
                outstr = f""
                pdf.cell(200,10, txt = outstr, ln = 1, align = 'L')
        
        pdf.output("/tmp/report.pdf")