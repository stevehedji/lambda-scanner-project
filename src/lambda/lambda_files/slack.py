import urllib.request
import zipfile
import requests

class Slack:
    
    slack_url = ""
    
    def __init__(self, slack_url: str):
        self.slack_url = slack_url
        
    # send_slack_report
    # A function to send the report to slack
    # inputs:
    # channel: str - the slack channel to send the report to
    # high: int - number of high severity vulnerabilities
    # med: int - number of medium severity vulnerabilities
    # low: int - number of low severity vulnerabilities
    # acc_id: str - the aws account id
    def send_slack_report(self, channel: str, high: int, med: int, low: int, acc_id: str):
        message_obj = {
            "channel":channel,
            "fallback": "Security Scan Results.",
            "pretext": f"Security Scan for {acc_id} returned the following results:",
            "color": "warning",
            "fields": [
                {
                    "title": "LOW Severity",
                    "value": low,
                    "short": False
                },
                {
                    "title": "MEDIUM Severity",
                    "value": med,
                    "short": False
                },
                {
                    "title": "HIGH Severity",
                    "value": high,
                    "short": False
                }
            ]
        }
        requests.post(self.slack_url, json = message_obj)

    # send_slack_error
    # A function to send an error to slack
    # inputs:
    # channel: str - the slack channel to send the report to
    # error: str - the error message
    def send_slack_error(self, channel: str, error: str):
        message_obj = {
            "channel":channel,
            "text": f"ERROR:\n{error}",
            "color": "warning"
        }
        requests.post(self.slack_url, json = message_obj)
    
    # send_slack_message
    # A function to send message to slack
    # inputs:
    # channel: str - the slack channel to send the report to
    # message: str - the message to send
    def send_slack_message(self, channel: str, message: str):
        message_obj = {
            "channel":channel,
            "text": message
        }
        requests.post(self.slack_url, json = message_obj)