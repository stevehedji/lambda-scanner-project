from slack import Slack
from ses import SES
from reportformatter import ReportFormatter
import json
import subprocess
import sys
import os
import boto3
import urllib.request
import zipfile
import requests

class Scan:
    
    # Initialise variables
    acc_id = ""
    slack_url = os.environ.get("SLACK_URL")
    slack_error = os.environ.get("SLACK_ERROR")
    slack_report = os.environ.get("SLACK_REPORT")
    client = boto3.client('lambda')
    
    def __init__(self, acc_id: str):
        self.acc_id = acc_id
        
    # start_scan
    # A function to start the scanning process
    def start_scan(self):
        slack = Slack(self.slack_url)
        try:
            slack.send_slack_message(self.slack_report, f"Scanning account: {self.acc_id}")
            total_high = 0
            total_low = 0
            total_med = 0
            print(f"Finding lambda functions for account {self.acc_id}...")
            # Get a list of functions in the AWS account
            func_response = self.client.list_functions()
            
            # Create a list of function names
            function_names = []
            for x in func_response["Functions"]:
                function_names.append(x["FunctionName"])
            
            all_functions = []
            
            print("Retrieving lambda info...")
            print("")
            total_functions = 0
            total_layers = 0
            # Find functions that use the Python Runtime
            for x in function_names:
                response = self.client.get_function(FunctionName=x)
                if "python" in response["Configuration"]["Runtime"] and x != "lambda-scanner":
                    total_functions += 1
                    all_functions.append([x, response["Code"]["Location"], response["Configuration"]["Runtime"]])
    
            # Get a list of layers in the AWS account
            list_layers = self.client.list_layers()
            # Create a list of layers
            for layer in list_layers['Layers']:
                if layer['LayerName'] != "lambda-scanner-layer":
                    total_layers += 1
                    get_layer = self.client.get_layer_version(LayerName=layer['LayerName'], VersionNumber=layer['LatestMatchingVersion']['Version'])
                    all_functions.append([f"Layer-{layer['LayerName']}", get_layer["Content"]["Location"], response["Configuration"]["Runtime"]])

            try:
                os.mkdir("/tmp/zips")
                os.mkdir("/tmp/reports")
            
            except:
                pass
            
            slack.send_slack_message(self.slack_report, f"Found {total_functions} Lambdas and {total_layers} Layers")
            print("Beginning Scan...")
            print("")
            os.chdir("/opt/python")
            cur_func = 0
            slack.send_slack_message(self.slack_report, "Downloading and extracting files")
            
            # Loop through all of the found functions
            for x in all_functions:
                cur_func += 1
                # Download the source code of the current function
                urllib.request.urlretrieve(x[1], f"/tmp/zips/{x[0]}.zip")
                # Unzip the source code
                with zipfile.ZipFile(f"/tmp/zips/{x[0]}.zip", "r") as zip_file:
                    zip_file.extractall(f"/tmp/zips/{x[0]}")
                    
                # Remove the temporary zip file
                subprocess.call(["rm", "-rf", f"/tmp/zips/{x[0]}.zip"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            file_count = 0
            # Count the total number of files
            for root_dir, cur_dir, files in os.walk(r'/tmp/zips'):
                file_count += len(files)
            print('file count:', file_count)
            slack.send_slack_message(self.slack_report, f"Scanning {file_count} files please wait...")
            # Scan every file that has been unzipped
            subprocess.call(["python3", "-m", "bandit", "-r", f"/tmp/zips" ,"-f", "json", "-o", f"/tmp/reports/report.json"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            
            # Open the report and format it into a pdf file
            repfile = open(f"/tmp/reports/report.json")
            rep_dict = json.load(repfile)
            repfile.close()
            slack.send_slack_message(self.slack_report, "Generating Report")
            ReportFormatter.format_report(rep_dict, self.acc_id)
            
            # Send an email with the report
            slack.send_slack_message(self.slack_report, "Sending Email")
            SES.send_email(os.environ.get("SENDER"), os.environ.get("RECIPIENT"), "ap-southeast-2", f"Scan report for AWS Acc: {self.acc_id}", "/tmp/report.pdf")
            
            # Report back to the slack channel
            total_high += rep_dict['metrics']['_totals']['SEVERITY.HIGH']
            total_med += rep_dict['metrics']['_totals']['SEVERITY.MEDIUM']
            total_low += rep_dict['metrics']['_totals']['SEVERITY.LOW']
            slack.send_slack_report(self.slack_report, total_high, total_med, total_low, self.acc_id)
            slack.send_slack_message(self.slack_report, "Scan complete!")

        except Exception as ex:
            slack.send_slack_error(self.slack_error, ex)