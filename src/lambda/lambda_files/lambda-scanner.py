from scan import Scan
import boto3

acc_id = boto3.client('sts').get_caller_identity().get('Account')

def lambda_handler(event, context):
    # Create the scanner object and start the scan
    scanner = Scan(acc_id)
    scanner.start_scan()
