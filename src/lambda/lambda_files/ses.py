import boto3
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
import os

class SES():

    # send_mail
    # A function to send an email report
    # inputs:
    # sender: str - the sending email address
    # recipient: str - the recieving email address
    # aws_region: str - the region to send the email from
    # subject: str - The subject line for the email
    # file_name: - The file to attach to the email
    def send_email(sender: str, recipient: str, aws_region: str, subject: str, file_name: str):
        # Create the body of the email
        BODY_TEXT = "Hello,\r\nPlease find the attached Report."
        BODY_HTML = """\
        <html>
        <head></head>
        <body>
        <h1>Hello!</h1>
        <p>Please find the attached Security scan Report.</p>
        </body>
        </html>
        """
        CHARSET = "utf-8"
        client = boto3.client('ses',region_name=aws_region)
        msg = MIMEMultipart('mixed')
        msg['Subject'] = subject 
        msg['From'] = sender 
        msg['To'] = recipient
        msg_body = MIMEMultipart('alternative')
        textpart = MIMEText(BODY_TEXT.encode(CHARSET), 'plain', CHARSET)
        htmlpart = MIMEText(BODY_HTML.encode(CHARSET), 'html', CHARSET)
        msg_body.attach(textpart)
        msg_body.attach(htmlpart)
        # Attach the report to the email
        att = MIMEApplication(open(file_name, 'rb').read())
        att.add_header('Content-Disposition','attachment',filename=f"{os.environ.get('REPORT_NAME')}.pdf")
        if os.path.exists(file_name):
            print("File exists")
        else:
            print("File does not exists")
        msg.attach(msg_body)
        msg.attach(att)
        # Send the email
        try:
            response = client.send_raw_email(
                Source=msg['From'],
                Destinations=[
                    msg['To']
                ],
                RawMessage={
                    'Data':msg.as_string(),
                }
            )
        except ClientError as e:
            return(e.response['Error']['Message'])
        else:
            return("Email sent! Message ID:", response['MessageId'])