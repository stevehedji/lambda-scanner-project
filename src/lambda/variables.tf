variable "function_name" {
    type = string
    description = "The name of the lambda function. Must match the file name"
}

variable "description" {
    type = string
    description = "Description of the lambda function"
}

variable "email_recipient" {
    type = string
    description = "Recipient email address for the report"
}
variable "report_name" {
    type = string
    description = "The filename for the report"
}
variable "sender" {
    type = string
    description = "Sender email address for the report"
}
variable "slack_error" {
    type = string
    description = "Slack channel to report errors to"
}
variable "slack_report" {
    type = string
    description = "Slack channel for general messages and report results"
}
variable "slack_url" {
    type = string
    description = "The webhook URL for slack"
}