# Lambda layer containing the pip3 packages required to run
resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "${path.module}/layer/bandit.zip"
  layer_name = "${var.function_name}-layer"
  source_code_hash = filebase64sha256("${path.module}/layer/bandit.zip")
  compatible_runtimes = ["python3.8"]
}

# Lambda function for the scanning application
resource "aws_lambda_function" "lambda" {
  filename         = "${path.module}/lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/lambda.zip")
  function_name    = var.function_name
  role             = aws_iam_role.lambda_role.arn
  handler          = "${var.function_name}.lambda_handler"
  runtime          = "python3.8"
  memory_size      = 512
  timeout          = 300
  description      = var.description
  depends_on = [aws_iam_role_policy_attachment.lambda_role_attach]
  layers = [aws_lambda_layer_version.lambda_layer.arn]
  architectures = ["arm64"]
  environment {
    variables = {
      RECIPIENT = var.email_recipient
      REPORT_NAME = var.report_name
      SENDER = var.sender
      SLACK_ERROR = var.slack_error
      SLACK_REPORT = var.slack_report
      SLACK_URL = var.slack_url
    }
  }
}
