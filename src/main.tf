# Import the module to create the lambda function
module "lambda_function"{
  source= "./lambda" # Source for the module
  function_name = "lambda-scanner" # The function name for the lambda function, must match the lambda file name
  description = "a lambda scanner function" # The description for the lambda function
  email_recipient = "<email>" # Email address to send to the report to
  report_name = "<report name>" # The filename for the report
  sender = "<email>" # Email address to send the report from
  slack_error = "<slack channel>" # The slack channel to send error messages to
  slack_report = "<slack channel>" # The slack channel to send general messages and the report results to
  slack_url = "https://hooks.slack.com/services/<webhook information>" # The URL for the slack webhook
}