# Declare Variables
VARS ?= vars/$(APP_NAME).tfvars
REQUIREMENTS = ./src/lambda/requirements.txt

# Initialisation to initialise the terraform backend
init:
	terraform -chdir=src init \
	-backend-config="dynamodb_table=$(TF_BACKEND_DYNAMO)" \
    -backend-config="bucket=$(TF_BACKEND_BUCKET)" \
	-backend-config="key=$(ENV)-$(APP_NAME)"

# Plan to output what is going to change on apply
plan:
	terraform -chdir=src plan -var-file=$(VARS) -out=.terraform-plan -lock=false
.PHONY: plan

# Apply the changes via terraform
apply:
	terraform -chdir=src apply .terraform-plan
.PHONY: apply

# Package the files needed to create the lambda layer
package_lambda:
	pip install -r $(REQUIREMENTS) -t ./src/lambda/layer/python --no-user
	cd src/lambda/layer/ && zip -FSr ./bandit.zip .
	cd src/lambda/lambda_files && zip -FSr ../lambda.zip .
.PHONY: package_lambda